package com.example.student.movieapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.student.movieapp.R;
import com.example.student.movieapp.models.Movie;
import com.example.student.movieapp.models.Result;
import com.example.student.movieapp.network.APIUtils;

import java.util.List;

import okhttp3.ResponseBody;

/**
 * Created by USER on 7/15/2017.
 */

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MyViewHolder> {

    private List<Movie> moviesList;
    private Context mContext;
    public MoviesAdapter(List<Movie> moviesList,Context context) {
        this.moviesList = moviesList;
        this.mContext=context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;
        ImageView imageView;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            genre = (TextView) view.findViewById(R.id.genre);
            year = (TextView) view.findViewById(R.id.year);
            imageView=(ImageView)view.findViewById(R.id.image);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()) //fills the layout
                .inflate(R.layout.movie_list_row, parent, false); //what layout are you inflating

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) { //gets whataever in the movielist and send it to the view
        Movie movie = moviesList.get(position);
        holder.title.setText(movie.getTitle());
        holder.genre.setText(movie.getOverview());
        holder.year.setText(movie.getReleaseDate());
        String path= APIUtils.IMAGE_BASE_URL+movie.getPosterPath();
        Glide.with(mContext)
                .load(path)
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }


    public void updateAdapter(List<Movie>movies){
        moviesList=movies;
        notifyDataSetChanged();
    }
}
